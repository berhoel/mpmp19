# MPMP19

My Python and C++ Solution for MPMP19 (Matt Parkers Math Puzzle 19) (<https://youtu.be/tBXGIXEV7tI>).

```shell
> time ./mpmp19 10000000
End at the 10000000th prime.
primes with up to 4096 bits.
The first 1 primes squared and added give 4, which is a multiple of 1. (333333 primes/s)
The first 19 primes squared and added give 24966, which is a multiple of 19. (240506 primes/s)
The first 37 primes squared and added give 263736, which is a multiple of 37. (250000 primes/s)
The first 455 primes squared and added give 1401992410, which is a multiple of 455. (180699 primes/s)
The first 509 primes squared and added give 2040870112, which is a multiple of 509. (177476 primes/s)
The first 575 primes squared and added give 3054955450, which is a multiple of 575. (172517 primes/s)
The first 20597 primes squared and added give 346739122490032, which is a multiple of 20597. (142340 primes/s)
The first 202717 primes squared and added give 499159078330000800, which is a multiple of 202717. (109633 primes/s)
The first 1864637 primes squared and added give 539391065522650998496, which is a multiple of 1864637. (82093.7 primes/s)
Count: 10000000, Time for the last 10000 primes: 0.157591 s; prime/s: 6.34554e+07
Time elapsed over all: 151.673 s; prime/s: 65931.2

real	2m45,085s
user	2m30,885s
sys	0m3,768s
```

```shell
> time python mpmp19.py 10000000
The first 1 primes squared and added give 4, which is a multiple of 1 (17.769357155833852 primes/s).
The first 19 primes squared and added give 24966, which is a multiple of 19 (337.1009031359007 primes/s).
The first 37 primes squared and added give 263736, which is a multiple of 37 (655.8160774091266 primes/s).
The first 455 primes squared and added give 1401992410, which is a multiple of 455 (7767.274431138051 primes/s).
The first 509 primes squared and added give 2040870112, which is a multiple of 509 (8643.801620711105 primes/s).
The first 575 primes squared and added give 3054955450, which is a multiple of 575 (9700.60442526053 primes/s).
The first 20597 primes squared and added give 346739122490032, which is a multiple of 20597 (95998.20817016388 primes/s).
The first 202717 primes squared and added give 499159078330000800, which is a multiple of 202717 (106517.67507063206 primes/s).
The first 1864637 primes squared and added give 539391065522650998496, which is a multiple of 1864637 (78682.95998544293 primes/s).
Count: 10000000, Time for the last 10000 primes: 0.14776742800000875 s; prime/s: 67667.46944194574) 
Time elapsed over all: 149.303341839 s; primes/s: 66977.73714088157s.

real	2m31,046s
user	2m26,946s
sys	0m2,413s
```
