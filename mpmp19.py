#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Solve "MPMP19: THE 19 CHALLENGE"
"""

import sys
import time

__date__ = "2020/11/28 13:45:43 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"

# Sieve of Eratosthenes
# Code by David Eppstein, UC Irvine, 28 Feb 2002
# http://code.activestate.com/recipes/117119/
def gen_primes():
    """Generate an infinite sequence of prime numbers."""
    # Maps composites to primes witnessing their compositeness.
    # This is memory efficient, as the sieve is not "run forward"
    # indefinitely, but only as long as required by the current
    # number being tested.
    #
    D = {}

    # The running integer that's checked for primeness
    q = 2

    while True:
        if q not in D:
            # q is a new prime.
            # Yield it and mark its first multiple that isn't
            # already marked in previous iterations
            #
            yield q
            D[q * q] = [q]
        else:
            # q is composite. D[q] is the list of primes that
            # divide it. Since we've reached q, we no longer
            # need it in the map, but we'll mark the next
            # multiples of its witnesses to prepare for larger
            # numbers
            #
            for p in D[q]:
                D.setdefault(p + q, []).append(p)
            del D[q]

        q += 1


END = int(sys.argv[1])

SUM = 0
rprt_cnt = 10000
start = time.process_time()
for cnt, p in enumerate(gen_primes(), 1):
    if not (cnt % rprt_cnt):
        print(
            80 * " \r"
            f"Count: {cnt}, Time for the last "
            f"{rprt_cnt} primes: {time.process_time() -start} s; "
            f"prime/s: {rprt_cnt / (time.process_time() -start)})",
            end="",
        )
        start = time.process_time()

    SUM += p * p
    if not (SUM % cnt):
        print(
            f"\rThe first {cnt} primes squared and added give {SUM}, which is a "
            f"multiple of {cnt} ({cnt/time.process_time()} primes/s)."
        )
    if cnt > END:
        break

print(f"\nTime elapsed over all: {time.process_time()} s; primes/s: {cnt/time.process_time()}s.")

# Local Variables:
# mode: python
# compile-command: "poetry run tox"
# time-stamp-pattern: "30/__date__ = \"%:y/%02m/%02d %02H:%02M:%02S %u\""
# End:









