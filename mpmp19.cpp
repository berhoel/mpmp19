/**
   \file
   \author Berthold Höllmann <berhoel@gmail.com>
   \copyright Copyright © 2020 by Berthold Höllmann
   \brief Solve "MPMP19: THE 19 CHALLENGE"
*/

#include "boost/multiprecision/cpp_int.hpp"
#include <boost/coroutine2/all.hpp>

#include <ctime>
#include <iostream>
#include <map>
#include <set>

namespace mp = boost::multiprecision;

static const int large_size{4096};
static const int huge_size{large_size * 2};

typedef mp::number<mp::cpp_int_backend<0, large_size, mp::unsigned_magnitude,
                                       mp::checked, void>>
    int_large_t;
typedef mp::number<mp::cpp_int_backend<0, huge_size, mp::unsigned_magnitude,
                                       mp::checked, void>>
    int_huge_t;
typedef boost::coroutines2::coroutine<int_large_t> coro_t;

void prime_sequence(coro_t::push_type &yield) {
  std::map<int_large_t, std::set<int_large_t>> D{};
  int_large_t q{2};
  for (;;) {
    if (D.find(q) == D.end()) { // q is prime
      yield(q);
      D[q * q] = {q};
    } else {
      // q is a composit. D[q] is the list of primes that divide it.
      for (auto p : D[q]) {
        if (D.find(p + q) == D.end()) {
          D[p + q] = {};
        }
        D[p + q].insert(p);
      }
      D.erase(q);
    }
    q += 1;
  }
}

int main(int argc, char *argv[]) {
  int_large_t cnt{1};
  int_huge_t sum{0};
  int_large_t end{100000};
  static const int_large_t rprt_cnt{10000};
  long double time_elapsed_s;

  if (argc > 1)
    end = int_large_t{argv[1]};
  std::cout << "End at the " << end << "th prime." << std::endl;

  std::cout << "primes with up to " << large_size << " bits." << std::endl;
  coro_t::pull_type seq(boost::coroutines2::fixedsize_stack(), prime_sequence);
  std::clock_t c_start{std::clock()};
  std::clock_t c_start_all{std::clock()};
  std::clock_t c_end{std::clock()};
  try {
    for (auto p : seq) {
      sum += p * p;
      if (!(cnt % rprt_cnt)) {
        c_end = std::clock();
        time_elapsed_s = 1. * (c_end - c_start) / CLOCKS_PER_SEC;
        std::cout << "                                                         "
                     "                       \rCount: "
                  << cnt << ", "
                  << "Time for the last " << rprt_cnt
                  << " primes: " << time_elapsed_s << " s; prime/s: "
                  << static_cast<long double>(cnt) / time_elapsed_s
                  << std::flush;

        c_start = std::clock();
      }
      if (!(sum % cnt)) {
        c_end = std::clock();
        time_elapsed_s = 1. * (c_end - c_start_all) / CLOCKS_PER_SEC;
        std::cout << "\r"
                  << "The first " << cnt << " primes squared and added give "
                  << sum << ", which is a multiple of " << cnt << ". ("
                  << static_cast<long double>(cnt) / time_elapsed_s
                  << " primes/s)" << std::endl;
      }
      if (cnt++ > end)
        break;
    }
  } catch (boost::exception &ex) {
    std::cout << "Range error at " << cnt << ". prime" << std::endl;
  }

  time_elapsed_s = 1. * (c_end - c_start_all) / CLOCKS_PER_SEC;
  std::cout << "\nTime elapsed over all: " << time_elapsed_s
            << " s; prime/s: " << static_cast<long double>(cnt) / time_elapsed_s
            << std::endl;

  return 0;
}

// Local Variables:
// mode: c++
// coding: utf-8
// c-file-style: "gcc"
// indent-tabs-mode: nil
// compile-command: "make"
// End:
